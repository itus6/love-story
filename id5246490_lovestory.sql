-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 06, 2018 at 12:14 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id5246490_lovestory`
--
CREATE DATABASE IF NOT EXISTS `id5246490_lovestory` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `id5246490_lovestory`;

-- --------------------------------------------------------

--
-- Table structure for table `aboutbackground`
--

DROP TABLE IF EXISTS `aboutbackground`;
CREATE TABLE `aboutbackground` (
  `id` int(11) NOT NULL,
  `image` varchar(20) DEFAULT NULL,
  `alt` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `aboutcontent`
--

DROP TABLE IF EXISTS `aboutcontent`;
CREATE TABLE `aboutcontent` (
  `id` int(11) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `content` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aboutcontent`
--

INSERT INTO `aboutcontent` (`id`, `title`, `content`) VALUES(1, 'Apprendre &Agrave; Nous Connaître', 'La compagnie a été établie en 1912 et a été la propriété familiale pendant près de vingt ans et a toujours été fière de servir la région avec les fleurs les plus fraîches. Nos conceptions uniques et service utile nous ont toujours mis une coupe au-dessus du reste. Nous cherchons continuellement à apporter de nouvelles idées à nos clients.');

-- --------------------------------------------------------

--
-- Table structure for table `aboutslide`
--

DROP TABLE IF EXISTS `aboutslide`;
CREATE TABLE `aboutslide` (
  `id` int(11) NOT NULL,
  `image` varchar(50) DEFAULT NULL,
  `alt` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aboutslide`
--

INSERT INTO `aboutslide` (`id`, `image`, `alt`) VALUES(1, 'table-mariage.jpg', 'table de mariage');
INSERT INTO `aboutslide` (`id`, `image`, `alt`) VALUES(2, 'mariage-en-plein-air.jpg', 'mariage en plein air');
INSERT INTO `aboutslide` (`id`, `image`, `alt`) VALUES(3, 'gateau-de-mariage.jpg', 'gateau de mariage');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `mail` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `mail`, `name`, `password`) VALUES(1, 'harivolafidisoa.rafidison@gmail.com', 'Fidisoa', 'fidisoa');
INSERT INTO `admin` (`id`, `mail`, `name`, `password`) VALUES(2, 'rojoraben.itu@gmail.com', 'Mr Rojo', 'rabenanahary');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

DROP TABLE IF EXISTS `blog`;
CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `datepublication` date DEFAULT NULL,
  `author` varchar(100) DEFAULT NULL,
  `article` text,
  `image` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `blogbackground`
--

DROP TABLE IF EXISTS `blogbackground`;
CREATE TABLE `blogbackground` (
  `id` int(11) NOT NULL,
  `image` varchar(20) DEFAULT NULL,
  `alt` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `image` varchar(20) DEFAULT NULL,
  `comments` text,
  `name` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `address` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `mail` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contactbackground`
--

DROP TABLE IF EXISTS `contactbackground`;
CREATE TABLE `contactbackground` (
  `id` int(11) NOT NULL,
  `image` varchar(20) DEFAULT NULL,
  `alt` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `logo`
--

DROP TABLE IF EXISTS `logo`;
CREATE TABLE `logo` (
  `id` int(11) NOT NULL,
  `image` varchar(20) DEFAULT NULL,
  `alt` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logo`
--

INSERT INTO `logo` (`id`, `image`, `alt`) VALUES(1, 'logo-love-story.png', 'logo love story organisateur de mariage');

-- --------------------------------------------------------

--
-- Table structure for table `slideaccueil`
--

DROP TABLE IF EXISTS `slideaccueil`;
CREATE TABLE `slideaccueil` (
  `id` int(11) NOT NULL,
  `image` varchar(50) DEFAULT NULL,
  `alt` varchar(100) DEFAULT NULL,
  `texts` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slideaccueil`
--

INSERT INTO `slideaccueil` (`id`, `image`, `alt`, `texts`) VALUES(1, 'couple-mariage.jpg', 'couple mariage', 'Un Mariage </br> Hors Du Commun');
INSERT INTO `slideaccueil` (`id`, `image`, `alt`, `texts`) VALUES(2, 'decoration-gateau-mariage.jpg', 'decoration de gateau de mariage', 'De Sublimes Decorations </br> Pour Le Grand Jour');
INSERT INTO `slideaccueil` (`id`, `image`, `alt`, `texts`) VALUES(3, 'decoration-table-mariage.jpg', 'decoration de table mariage', 'De Jolis Bouquets &Agrave; La Main');

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` text NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `skype` varchar(25) NOT NULL,
  `facebook` varchar(100) DEFAULT NULL,
  `twitter` varchar(100) DEFAULT NULL,
  `instagram` varchar(100) DEFAULT NULL,
  `image` varchar(20) DEFAULT NULL,
  `alt` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id`, `name`, `description`, `phone`, `mail`, `skype`, `facebook`, `twitter`, `instagram`, `image`, `alt`) VALUES(1, 'Jessica Newman', 'Je suis un fondateur et un planificateur principal de la société Your Effortless Event. J\'aime l\'excitation de participer à la planification d\'un événement depuis le début et de voir la vision devenir une réalité.\r\n<br>\r\nJ\'investis personnellement dans chaque mariage sur lequel je travaille; En fait, la nuit précédant un grand événement, je suis rempli de l\'excitation et de l\'énergie que j\'avais la veille de mon propre mariage ...', '+261 34 34 335 25', 'jessica.newman@love-story.mg', 'jessica.newman', 'https://www.facebook.com/harivolafidisoa.rafidison', 'https://twitter.com/fidsurafidson', 'https://www.instagram.com/fidsurafidson', 'jessica-newman.jpg', 'Jessica Newman');
INSERT INTO `team` (`id`, `name`, `description`, `phone`, `mail`, `skype`, `facebook`, `twitter`, `instagram`, `image`, `alt`) VALUES(2, 'Pamela Jones', 'J\'aime quand les gens réalisent leurs rêves. J\'aime voir des sourires heureux sur leurs visages. J\'apprécie de voir des mariés heureux et leurs parents lors d\'une des journées les plus importantes de leur vie.\r\n<br>\r\nQuand la mariée et le marié sont joyeux, heureux et très reconnaissants, cela signifie que j\'ai fait mon travail de la meilleure façon possible.\r\n<br>\r\nJ\'aime mon travail et reçois du plaisir quand je vois le bonheur des autres ...', '+32 32 359 32', 'pamela.jone@love-story.com', 'pamela.jones', 'https://www.facebook.com/harivolafidisoa.rafidison', 'https://twitter.com/fidsurafidson', 'https://www.instagram.com/fidsurafidson', 'pamela-jones.jpg', 'Pamela Jones');
INSERT INTO `team` (`id`, `name`, `description`, `phone`, `mail`, `skype`, `facebook`, `twitter`, `instagram`, `image`, `alt`) VALUES(3, 'Elisabeth Miles', 'Tout mariage a le potentiel d\'être joli, mais c\'est l\'attention portée aux détails qui donne vraiment de la vie à un événement. Je sais que certaines choses doivent être couvertes lors de la planification d\'un événement.\r\n\r\nEn tant que membre de l\'équipe, je fais tout ce qui est en mon pouvoir pour que votre mariage continue comme prévu et préparé, quelles que soient les situations d\'urgence qui se produisent le jour de votre mariage ...', '+33 33 256 95', 'elisabeth.miles@love-story.mg', 'elisabeth.miles', 'https://www.facebook.com/harivolafidisoa.rafidison', 'https://twitter.com/fidsurafidson', 'https://www.instagram.com/fidsurafidson', 'elisabeth-miles.jpg', 'Elisabeth Miles');

-- --------------------------------------------------------

--
-- Table structure for table `teambackground`
--

DROP TABLE IF EXISTS `teambackground`;
CREATE TABLE `teambackground` (
  `id` int(11) NOT NULL,
  `image` varchar(20) DEFAULT NULL,
  `alt` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teamdescription`
--

DROP TABLE IF EXISTS `teamdescription`;
CREATE TABLE `teamdescription` (
  `id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `motdudirecteur` text,
  `image` varchar(50) DEFAULT NULL,
  `alt` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teamdescription`
--

INSERT INTO `teamdescription` (`id`, `title`, `motdudirecteur`, `image`, `alt`) VALUES(1, 'Notre &Eacute;quipe Planifie Et Conçoit Des Mariage', 'Les mariages sont des événements importants dans la vie des gens et, en tant que tels, les couples sont souvent prêts à dépenser une somme considérable d\'argent pour s\'assurer que leurs mariages sont bien organisés. Les membres de notre équipe sont souvent utilisés par les couples qui travaillent de longues heures et ont peu de temps libre pour l\'approvisionnement et la gestion des lieux de mariage.\r\n', 'directrice-love-story.jpg', 'directrice love story');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aboutbackground`
--
ALTER TABLE `aboutbackground`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aboutcontent`
--
ALTER TABLE `aboutcontent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aboutslide`
--
ALTER TABLE `aboutslide`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogbackground`
--
ALTER TABLE `blogbackground`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contactbackground`
--
ALTER TABLE `contactbackground`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logo`
--
ALTER TABLE `logo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slideaccueil`
--
ALTER TABLE `slideaccueil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teambackground`
--
ALTER TABLE `teambackground`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teamdescription`
--
ALTER TABLE `teamdescription`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aboutbackground`
--
ALTER TABLE `aboutbackground`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `aboutcontent`
--
ALTER TABLE `aboutcontent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `aboutslide`
--
ALTER TABLE `aboutslide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blogbackground`
--
ALTER TABLE `blogbackground`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contactbackground`
--
ALTER TABLE `contactbackground`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logo`
--
ALTER TABLE `logo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `slideaccueil`
--
ALTER TABLE `slideaccueil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `teambackground`
--
ALTER TABLE `teambackground`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teamdescription`
--
ALTER TABLE `teamdescription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
