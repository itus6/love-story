<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

		<title>A propos</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/nprogress.css" rel="stylesheet">
    <link href="css/green.css" rel="stylesheet">
  
    <link href="css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <link href="css/jqvmap.min.css" rel="stylesheet"/>
    <link href="css/daterangepicker.css" rel="stylesheet">

    <link href="css/custom.min.css" rel="stylesheet">
	
	<?php require ('fonctions.php') ?>
	
	<?php
		$id=$_GET['id'];
		session_start();
		
	?>
	<?php $admins = get_NomAdmin($id);
	foreach ($admins as $admin) {
		$nom=$admin['name'];
	} 
	$about = findAboutContent();
	$slides = findAboutSlide();
	?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            

            <div class="clearfix"></div>

            <!-- L'admin connecte -->
				<div class="profile clearfix">
					<div class="profile_info">
						<span align="center">Bienvenue</span>
						<h2><?php echo $nom; ?></h2>
					</div>
				</div>
            <!-- /menu -->
            <br/>
            <!-- sidebar menu -->
            <?php include('sidebar-menu.php'); ?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="D&eacute;connexion" href="deconnexion.php">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="glyphicon glyphicon-align-justify"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/placeholder.png" alt=""> <?php echo $nom; ?>
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          
            <div class="page-title">
              
            </div>
            <div class="clearfix"></div>
			
            <div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
							 <div class="x_title">
								<h2>Slide</h2>
								
								<div class="clearfix"></div>
								<div class="table-responsive">
								  <table class="table">
									<thead>
										<tr>
											<th> Image </th>
											<th> Alt </th>
											<th>  </th>
										</tr>
									</thead>
									<tbody>
									<?php foreach ($slides as $slide){?>
										<tr>
											<td> <img width="25%" src="../front/images/<?php echo $slide['image']?>"> </td>
											<td> <?php echo $slide['alt']?> </td>
											<td> <button type="button" class="btn btn-warning">Modifier</button> </td>
										</tr>
									<?php } ?>
									</tbody>
								  </table>
								</div>
							  </div>
							  <div class="x_title">
								<h2>Description</h2>
								
								<div class="clearfix"></div>
								<div class="table-responsive">
								  <table class="table">
									<thead>
										<tr>
											<th> Titre </th>
											<th> Description </th>
											<th>  </th>
										</tr>
									</thead>
									<tbody>
									<?php foreach ($about as $description){?>
										<tr>
											<td> <?php echo $description['title']?> </td>
											<td> <?php echo $description['content']?> </td>
											<td> <button type="button" class="btn btn-warning">Modifier</button> </td>
										</tr>
									<?php } ?>
									</tbody>
								  </table>
								</div>
							  </div>

										
									
							  </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	
	    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>

    <!-- Custom Theme Scripts -->

	    <script src="js/custom.min.js"></script>
	
  </body>
</html>
