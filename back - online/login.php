<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> Login </title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="css/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
     
      <div class="login_wrapper">
        <div class="animate form login_form">
        
          <section class="login_content">
            
              <h1>Authentification</h1>
			  <form action="connexion.php" method="post">
              <div>
                <input type="text" class="form-control" name="mail" placeholder="Mail" required="" />
              </div>
              <div>
                <input type="password" class="form-control" name="password" placeholder="Mot de passe" required="" />
              </div>
              <div>
                <input type="submit" value="Connexion" class="btn btn-default submit" />
              </div>
			  </form>

              <div class="clearfix"></div>

              <div class="separator">
               

                <div class="clearfix"></div>
                <br />

              </div>
            
          </section>
        </div>

      </div>
    </div>
  </body>
</html>
