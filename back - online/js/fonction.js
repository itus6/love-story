function color(lien,couleur) {
	lien.style.color=couleur;
}

function majuscule(mot,id) {
	var element = document.getElementById(id);
	element.innerHTML=mot.toUpperCase();
}

function ouvreFerme() {
	var daty=new Date();
	var heure=daty.getHours();
	var minute=daty.getMinutes();
	//var heureD=heureDiffusion.split(":");
	var reste=14-heure;
	if (reste>0) {
		if (reste==1){
			var resteMin=60-minute;
			document.getElementById("ouvreFerme").innerHTML="Ouvre dans "+resteMin+" minutes";
		}
		else {
			document.getElementById("ouvreFerme").innerHTML="Ouvre dans "+reste+" heures";
		}
	}
	else if (reste<=0) {
		if (heure<22 && heure>=14) {
			if (heure==21) {
				var resteMin=60-minute;
				document.getElementById("ouvreFerme").innerHTML="Ferme dans "+resteMin+" minutes";
			}
			else {
				document.getElementById("ouvreFerme").innerHTML="Actuellement ouvert";
			}
		}
		else if(heure>=22) {
			document.getElementById("ouvreFerme").innerHTML="Ouvre demain";
		}
	}
}

function on(id) {
    document.getElementById("overlay").style.display = "block";
	document.forms["reservation"].elements["id"].value=id;
}

function off() {
    document.getElementById("overlay").style.display = "none";
}

function confirmation(id,idAdmin) {
	var r = confirm("Voulez-vous vraiment supprimer ?");
	if (r == true) {
		location.replace("deleteFilm.php?id="+id+"&idAdmin="+idAdmin+"");
	} else {
		location.replace("listeFilm.php?id="+idAdmin+"");
	}
}

function confirmationSeance(id,seance,idAdmin) {
	var r = confirm("Voulez-vous vraiment supprimer ?");
	if (r == true) {
		location.replace("deleteSeance.php?id="+id+"&seance="+seance+"&idAdmin="+idAdmin+"");
	} else {
		location.replace("listeSeance.php?id="+idAdmin+"");
	}
}

function bandeAnnonce(lien) {
	document.getElementById("bandeAnnonce").src = "https://www.youtube.com/embed/"+lien;
	document.getElementById("eto").innerHTML=lien;
}
