<?php
/**
 * Created by PhpStorm.
 * User: Harivola Fidisoa RAF
 * Date: 31/03/2018
 * Time: 21:14
 */
?>
<footer class="footer_wrap widget_area scheme_original">
    <!--.footer_wrap_inner -->
    <div class="footer_wrap_inner widget_area_inner">
        <!--.content_wrap -->
        <div class="content_wrap">
            <aside id="lovestory_widget_socials-2" class="widget_number_1 widget widget_socials">
                <div class="widget_inner">
                    <div class="logo">
                        <a href="accueil"><img src="images/logo_2x.png" class="logo_main" alt=""></a>
                    </div>
                    <div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny">
                        <div class="sc_socials_item"><a href="https://www.facebook.com/harivolafidisoa.rafidison" class="social_icons social_facebook"><span class="icon-facebook"></span></a></div><div class="sc_socials_item"><a href="https://twitter.com/fidsurafidson" class="social_icons social_twitter"><span class="icon-twitter"></span></a></div><div class="sc_socials_item"><a href="https://www.instagram.com/fidsurafidson" class="social_icons social_instagramm"><span class="icon-instagramm"></span></a></div><div class="sc_socials_item"><a href="https://plus.google.com/117807525761284717688" class="social_icons social_gplus"><span class="icon-gplus"></span></a></div>
                    </div>
                </div>
            </aside>
        </div>
        <!-- end .content_wrap -->
    </div>
    <!-- end .footer_wrap_inner -->
</footer>
<!-- end .footer_wrap -->

<div class="copyright_wrap copyright_style_text  scheme_original">
    <div class="copyright_wrap_inner">
        <div class="content_wrap">
            <div class="copyright_text">
                <p>RAFIDISON Harivola Fidisoa ETU00512 Promo 9A &copy; 2018 </p>
            </div>
        </div>
    </div>
</div>
