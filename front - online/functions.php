<?php
    function dbconnect(){
		$user = 'id5246490_fids';
		$pass = 'lovestory';
		$dsn = 'mysql:host=localhost;dbname=id5246490_lovestory';
		static $dbh = null;
		if($dbh === null){
			$dbh = new PDO($dsn, $user, $pass);
			$dbh->exec("set names utf8");
		}
		return $dbh;
	}

    function findLogo(){
        $resultats=dbconnect()->query("select * from logo");
        $resultats->setFetchMode(PDO::FETCH_ASSOC);
        $resultats->execute();
        $ligne=$resultats->fetchAll();
        return $ligne;
    }

    function findSlide(){
        $resultats=dbconnect()->query("select * from slideaccueil");
        $resultats->setFetchMode(PDO::FETCH_ASSOC);
        $resultats->execute();
        $ligne=$resultats->fetchAll();
        return $ligne;
    }

    function findAboutContent(){
        $resultats=dbconnect()->query("select * from aboutcontent");
        $resultats->setFetchMode(PDO::FETCH_ASSOC);
        $resultats->execute();
        $ligne=$resultats->fetchAll();
        return $ligne;
    }

    function findAboutSlide(){
        $resultats=dbconnect()->query("select * from aboutslide");
        $resultats->setFetchMode(PDO::FETCH_ASSOC);
        $resultats->execute();
        $ligne=$resultats->fetchAll();
        return $ligne;
    }

    function findTeam(){
        $resultats=dbconnect()->query("select * from team");
        $resultats->setFetchMode(PDO::FETCH_ASSOC);
        $resultats->execute();
        $ligne=$resultats->fetchAll();
        return $ligne;
    }
	
	function findTeamById($id){
        $resultats=dbconnect()->query("select * from team where id=".$id."");
        $resultats->setFetchMode(PDO::FETCH_ASSOC);
        $resultats->execute();
        $ligne=$resultats->fetchAll();
        return $ligne;
    }

    function findGallery(){
        $resultats=dbconnect()->query("select * from galleryimage");
        $resultats->setFetchMode(PDO::FETCH_ASSOC);
        $resultats->execute();
        $ligne=$resultats->fetchAll();
        return $ligne;
    }

    function findComments(){
        $resultats=dbconnect()->query("select * from comments");
        $resultats->setFetchMode(PDO::FETCH_ASSOC);
        $resultats->execute();
        $ligne=$resultats->fetchAll();
        return $ligne;
    }

    function findArticles(){
        $resultats=dbconnect()->query("select * from blog");
        $resultats->setFetchMode(PDO::FETCH_ASSOC);
        $resultats->execute();
        $ligne=$resultats->fetchAll();
        return $ligne;
    }

    function findPlans(){
        $resultats=dbconnect()->query("select * from eventsplans");
        $resultats->setFetchMode(PDO::FETCH_ASSOC);
        $resultats->execute();
        $ligne=$resultats->fetchAll();
        return $ligne;
    }

    function findContact(){
        $resultats=dbconnect()->query("select * from contact");
        $resultats->setFetchMode(PDO::FETCH_ASSOC);
        $resultats->execute();
        $ligne=$resultats->fetchAll();
        return $ligne;
    }
	
	function findDescriptionTeam(){
		$resultats=dbconnect()->query("select * from teamdescription");
        $resultats->setFetchMode(PDO::FETCH_ASSOC);
        $resultats->execute();
        $ligne=$resultats->fetchAll();
        return $ligne;
	}
    
?>