<?php
	require('functions.php');
	include('logo.php');
?>
			<div class="top_panel_bottom">
                        <div class="content_wrap clearfix">
                            <nav class="menu_main_nav_area menu_hover_fade">
                                <ul id="menu_main" class="menu_main_nav">
                                    <li class="menu-item  menu-item-has-children current-menu-ancestor">
                                        <a href="index.php"><span>Accueil</span></a>
                                    </li>
                                    <li class="menu-item"><a href="nous-connaitre.php"><span>&Agrave; propos</span></a>
                                        
                                    </li>
                                    
                                    <li class="menu-item"><a href="notre-equipe.php"><span>&Eacute;quipe</span></a></li>
                                    <li class="menu-item "><a href="articles.php"><span>Articles</span></a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
            <div class="top_panel_title title_present">
                <div class="top_panel_title_inner title_present bg-breadcrumbs">
                    <div class="content_wrap">
                        <h1 class="page_title">Contacts</h1>
                        <div class="breadcrumbs">
                            <a class="breadcrumbs_item home" href="index.html">Home</a><span class="breadcrumbs_delimiter"></span><span class="breadcrumbs_item current">Contacts</span>
                        </div>	
                    </div>
                </div>
            </div>
            <div class="page_content_wrap page_paddings_no scheme_original">
                <div class="content">
                   <!-- .post_item_single .page .type-page  -->
                    <article class="post_item_single page type-page">
                       <!-- .post_content -->
                        <div class="post_content">
                           <section class="no-col-padding">
                                <div class="container-fluid">
                                    <div class="columns_wrap">
                                        <div class="column_container column-1_1">
                                            <div class="column-inner">
                                                <div class="m_wrapper">
                                                    <div id="sc_googlemap_1_wrap" class="sc_googlemap sc-googlemap-param" data-zoom="14" data-style="simple">
                                                        <div id="sc_googlemap_1_1" class="sc_googlemap_marker" 
                                                             data-title="" 
                                                             data-description="Los Angeles" 
                                                             data-address="Los Angeles" 
                                                             data-latlng="" 
                                                             data-point="images/location.png">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                             <section class="no-col-padding">
                                <div class="content_wrap">
                                    <div class="columns_wrap">
                                        <div class="column_container column-1_1">
                                            <div class="column-inner">
                                                <div class="m_wrapper">
                                                    <div class="sc_section contact_block sc_section_block sc-section-bg-contacts">
                                                        <div class="sc_section_inner">
                                                            <div class="sc_section_overlay">
                                                                <div class="sc_section_content padding_on">
                                                                    <div class="sc_section_content_wrap">
                                                                        <div class="columns_wrap sc_columns columns_nofluid sc_columns_count_3">
                                                                            <div class="column-1_3 sc_column_item odd first sc-contacts-column-custom"><span class="sc_icon icon-icon4 sc_icon_shape_round sc-contacts-transform"></span>
                                                                                <h5 class="sc_title sc_title_regular sc_align_center sc-contacts-custom-h5">Address</h5>
                                                                                <div class="m_text_column m_content_element ">
                                                                                    <div class="m_wrapper">
                                                                                        <p>123 New Lenox,
                                                                                            <br /> Chicago, IL 60606</p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="sc-contacts-emptyspace" ></div>
                                                                            </div><div class="column-1_3 sc_column_item even sc-contacts-column-custom"><span class="sc_icon icon-icon5 sc_icon_shape_round sc-contacts-transform"></span>
                                                                                <h5 class="sc_title sc_title_regular sc_align_center sc-contacts-custom-h5">Phone</h5>
                                                                                <div class="m_text_column m_content_element ">
                                                                                    <div class="m_wrapper">
                                                                                        <p>(800)-456-7890</p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="sc-contacts-emptyspace"></div>
                                                                            </div><div class="column-1_3 sc_column_item odd sc-contacts-column-custom"><span class="sc_icon icon-icon6 sc_icon_shape_round sc-contacts-transform"></span>
                                                                                <h5 class="sc_title sc_title_regular sc_align_center sc-contacts-custom-h5">Email address</h5>
                                                                                <div class="m_text_column m_content_element ">
                                                                                    <div class="m_wrapper">
                                                                                        <p>info@yoursite.com</p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="sc-contacts-emptyspace"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>	
                            </section>
                            <section class="no-col-padding">
                                <div class="content_wrap">
                                    <div class="columns_wrap">
                                        <div class="column_container column-1_1">
                                            <div class="column-inner">
                                                <div class="m_wrapper">
                                                    <div class="sc_form_wrap">
                                                        <div class="sc_form sc_form_style_form_1 aligncenter sc-contacts-form">
                                                            <h3 class="sc_form_title sc_item_title sc_item_title_without_descr">Give Us a Feedback</h3>
                                                            <h6 class="sc_form_subtitle sc_item_subtitle">contact form</h6>
                                                            <form class="sc_input_hover_default" data-formtype="form_1" method="post" action="http://lovestory-html.themerex.net/include/contact-form.php">
                                                                <div class="sc_form_info columns_wrap">
                                                                    <div class="sc_form_item sc_form_field label_over column-1_2">
                                                                        <input id="sc_form_username" type="text" name="username" placeholder="Name *" aria-required="true">
                                                                    </div><div class="sc_form_item sc_form_field label_over column-1_2">
                                                                        <input id="sc_form_email" type="text" name="email" placeholder="E-mail *" aria-required="true">
                                                                    </div>
                                                                    <div class="sc_form_item sc_form_message column-1_1">
                                                                        <textarea id="sc_form_message" name="message" placeholder="Message" aria-required="true"></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="sc_form_item sc_form_button">
                                                                    <button>Send Message</button>
                                                                </div>
                                                                <div class="result sc_infobox"></div>
                                                            </form> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>	
                                </div>    
                            </section>                        
                        </div>
                        <!-- end .post_content -->
                    </article>
                    <!-- end .post_item_single .page .type-page  -->
                </div>
                <!-- end .content> -->
            </div>
            <!-- end .page_content_wrap> -->
            <!-- .footer_wrap -->
            <footer class="footer_wrap widget_area scheme_original">
                <!-- .footer_wrap_inner -->
                <div class="footer_wrap_inner widget_area_inner">
                    <!-- .content_wrap -->
                    <div class="content_wrap">
                        <aside class="widget widget_socials">
                            <div class="widget_inner">
                                <div class="logo">
                                    <a href="#"><img src="images/logo_2x.png" class="logo_main" alt=""></a>
                                </div>
                                <div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny">
                                    <div class="sc_socials_item"><a href="#" class="social_icons social_facebook"><span class="icon-facebook"></span></a></div><div class="sc_socials_item"><a href="#" class="social_icons social_twitter"><span class="icon-twitter"></span></a></div><div class="sc_socials_item"><a href="#" class="social_icons social_instagramm"><span class="icon-instagramm"></span></a></div><div class="sc_socials_item"><a href="#" class="social_icons social_gplus"><span class="icon-gplus"></span></a></div>
                                </div>
                            </div>
                        </aside>
                    </div>
                    <!-- end .content_wrap -->
                </div>
                <!-- end .footer_wrap_inner -->
            </footer>
              <!-- end .footer_wrap -->
            <div class="copyright_wrap copyright_style_text  scheme_original">
                <div class="copyright_wrap_inner">
                    <div class="content_wrap">
                        <div class="copyright_text">
                            <p>ThemeREX &copy; 2017 All Rights Reserved <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end .page_wrap -->
    </div>
    <!-- end .body_wrap -->
    <a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>
    
    <script type='text/javascript' src='js/vendor/jquery-3.1.1.js'></script>
    <script type='text/javascript' src='js/vendor/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='js/vendor/photostack/modernizr.min.js'></script>
    <script type='text/javascript' src='js/vendor/superfish.js'></script>
    <script type="text/javascript" src="js/custom/_main.js"></script>
    <script type='text/javascript' src='js/custom/core.utils.js'></script>
    <script type='text/javascript' src='js/custom/core.init.js'></script>
    <script type='text/javascript' src='js/custom/template.init.js'></script>
    <script type='text/javascript' src='js/custom/template.shortcodes.js'></script>
    <script type='text/javascript' src='js/vendor/magnific/jquery.magnific-popup.min.js'></script>
    <script type='text/javascript' src='js/vendor/core.messages/core.messages.js'></script>
    <script type='text/javascript' src='http://maps.google.com/maps/api/js?key=AIzaSyA2RBVOv2ey-tb7TT1-_JCo8NbkRbYQBfc&amp;rnd=648824398'></script>
    <script type='text/javascript' src='js/custom/core.googlemap.js'></script>

    

</body>

<!-- Mirrored from lovestory-html.themerex.net/contacts.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 31 Mar 2018 08:08:32 GMT -->
</html>