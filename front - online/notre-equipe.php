<?php 
require('functions.php');
$introduction = findDescriptionTeam();
$teams = findTeam();
include ('logo.php')?>
                    <div class="top_panel_bottom">
                        <div class="content_wrap clearfix">
                            <nav class="menu_main_nav_area menu_hover_fade">
                                <ul id="menu_main" class="menu_main_nav">
                                    <li class="menu-item">
                                        <a href="accueil"><span>Accueil</span></a>
                                    </li>
                                    <li class="menu-item"><a href="nous-connaitre"><span>&Agrave; propos</span></a>
                                        
                                    </li>
                                    
                                    <li class="menu-item current-menu-ancestor"><a href="notre-equipe"><span>&Eacute;quipe</span></a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
            
            
            <div class="top_panel_title title_present">
                <div class="top_panel_title_inner title_present bg-breadcrumbs">
                    <div class="content_wrap">
                        <h1 class="page_title">Notre &Eacute;quipe</h1>
                        <div class="breadcrumbs">
                            <a class="breadcrumbs_item home" href="index.php">Accueil</a><span class="breadcrumbs_delimiter"></span><span class="breadcrumbs_item current">Notre &Eacute;quipe</span>
                        </div>	
                    </div>
                </div>
            </div>		
            <!--.page_content_wrap-->        
			<div class="page_content_wrap page_paddings_no">
               <!-- .content-->
                <div class="content">
                   <!--.post_item .post_item_single .page .type-page -->
                    <article class="post_item post_item_single page type-page">
                       <!--.post_content -->
                        <div class="post_content">
                            <section class="no-col-padding">
                                <div class="container-fluid sc_our-team-bg">    
                                    <div class="content_container">
                                        <div class="columns_wrap">
                                            <div class="column_container column-1_1">
                                                <div class="column-inner">
                                                    <div class="m_wrapper">
                                                        <div class="columns_wrap sc_columns columns_nofluid sc_columns_count_9 sc_our-team-a-margin">
                                                            <div class="column-5_9 sc_column_item sc_column_item_1 odd first span_5">
                                                                <h6 class="sc_title sc_title_regular margin_top_null sc_our-team-title-margin">Les mots de la directrice</h6>
                                                                <h2 class="sc_title sc_title_iconed margin_top_tiny margin_bottom_null"><?php echo $introduction[0]['title'] ?> <span class="sc_title_icon sc_title_icon_bottom  sc_title_icon_small"><img src="images/vector-smart-object-copy-12.png" alt="" /></span></h2>
                                                                <div class="m_text_column m_content_element ">
                                                                    <div class="m_wrapper">
                                                                        <p><?php echo $introduction[0]['motdudirecteur'] ?> </p>
                                                                    </div>
                                                                </div>
                                                                <div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny margin_top_medium margin_bottom_small">
                                                                    <div class="sc_socials_item"><a href="https://www.facebook.com/harivolafidisoa.rafidison" class="social_icons social_facebook"><span class="icon-facebook"></span></a></div><div class="sc_socials_item"><a href="https://twitter.com/fidsurafidson" class="social_icons social_twitter"><span class="icon-twitter"></span></a></div><div class="sc_socials_item"><a href="https://www.instagram.com/fidsurafidson" class="social_icons social_instagramm"><span class="icon-instagramm"></span></a></div><div class="sc_socials_item"><a href="https://plus.google.com/117807525761284717688" class="social_icons social_gplus"><span class="icon-gplus"></span></a></div>
                                                                </div>
                                                            </div><div class="column-4_9 sc_column_item sc_column_item_6 even span_4">
                                                                <figure class="sc_image  sc_image_shape_square"><img src="images/<?php echo $introduction[0]['image'] ?>" alt="<?php echo $introduction[0]['alt'] ?>" /></figure>
                                                            </div>
                                                        </div>    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </section>  
                            <section class="no-col-padding">
                                <div class="content_container">
                                    <div class="columns_wrap">
                                        <div class="column_container column-1_1">
                                            <div class="column-inner">
                                                 <div class="m_wrapper">
                                                   <!--.sc_team_wrap -->
                                                    <div class="sc_team_wrap">
                                                       <!--.sc_team -->
                                                        <div class="sc_team sc_team_style_team-1  margin_bottom_huge sc_our-team-b-margin">
                                                            <h3 class="sc_team_title sc_item_title sc_item_title_without_descr">Une Bonne &Eacute;quipe De Professionnels</h3>
                                                            <h6 class="sc_team_subtitle sc_item_subtitle">équipe</h6>
                                                            <div class="sc_columns columns_wrap">
 															<?php foreach ($teams as $team){ ?>
                                                                <div class="column-1_3 column_padding_bottom" style="width:32%">
                                                                   <div class="sc_team_item" >
                                                                        <div class="sc_team_item_avatar"><img class="post-image" alt="<?php echo $team['alt'] ?>" src="images/<?php echo $team['image'] ?>"></div>
                                                                        <div class="sc_team_item_info">
                                                                            <h5 class="sc_team_item_title"><a href="membre-equipe-<?php echo $team["id"];?>"><?php echo $team['name'] ?></a></h5>
                                                                            <div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny">
                                                                                <div class="sc_socials_item"><a href="<?php echo $team['facebook'] ?>" class="social_icons social_facebook"><span class="icon-facebook"></span></a></div><div class="sc_socials_item"><a href="<?php echo $team['twitter'] ?>" class="social_icons social_twitter"><span class="icon-twitter"></span></a></div><div class="sc_socials_item"><a href="<?php echo $team['instagram'] ?>" class="social_icons social_instagramm"><span class="icon-instagramm"></span></a></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
															<?php } ?>
                                                            </div>
                                                        </div>
                                                        <!-- end .sc_team -->
                                                    </div>
                                                    <!-- end .sc_team_wrap -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>	
                            </section>
                        </div>
                        <!-- end .post_content -->
                    </article>
                    <!-- end .post_item .post_item_single .page .type-page -->
                </div>
                <!-- end .content-->
            </div>
            <!-- end .page_content_wrap-->
			<!-- .footer_wrap -->
            
            <!-- end .footer_wrap -->
            <?php include('footer.php')?>
        </div>
        <!-- end .page_wrap -->
    </div>
    <!-- end .body_wrap -->
    <a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>
 
    <script type='text/javascript' src='js/vendor/jquery-3.1.1.js'></script>
    <script type='text/javascript' src='js/vendor/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='js/vendor/photostack/modernizr.min.js'></script>
    <script type='text/javascript' src='js/vendor/superfish.js'></script>
    <script type="text/javascript" src="js/custom/_main.js"></script>
    <script type='text/javascript' src='js/custom/core.utils.js'></script>
    <script type='text/javascript' src='js/custom/core.init.js'></script>
    <script type='text/javascript' src='js/custom/template.init.js'></script>
    <script type='text/javascript' src='js/custom/template.shortcodes.js'></script>
    <script type='text/javascript' src='js/vendor/magnific/jquery.magnific-popup.min.js'></script>
    <script type='text/javascript' src='js/vendor/core.messages/core.messages.js'></script>
 
</body>

<!-- Mirrored from lovestory-html.themerex.net/our-team.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 31 Mar 2018 13:06:21 GMT -->
</html>	     						