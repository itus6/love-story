<?php 
require('functions.php');
$about = findAboutContent();
$slide = findAboutSlide();
include ('logo.php')?>
                     <div class="top_panel_bottom">
                        <div class="content_wrap clearfix">
                            <nav class="menu_main_nav_area menu_hover_fade">
                                <ul id="menu_main" class="menu_main_nav">
                                    <li class="menu-item ">
                                        <a href="accueil"><span>Accueil</span></a>
                                    </li>
                                    <li class="menu-item current-menu-ancestor"><a href="nous-connaitre"><span>&Agrave; propos</span></a>
                                        
                                    </li>
                                    
                                    <li class="menu-item"><a href="notre-equipe"><span>&Eacute;quipe</span></a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
            
            	
            <div class="top_panel_title title_present">
                <div class="top_panel_title_inner title_present bg-breadcrumbs">
                    <div class="content_wrap">
                        <h1 class="page_title">&Agrave; Propos De Nous</h1>
                        <div class="breadcrumbs">
                            <a class="breadcrumbs_item home" href="index.php">Accueil</a><span class="breadcrumbs_delimiter"></span><span class="breadcrumbs_item current">&Agrave; Propos</span>
                        </div>	
                    </div>
                </div>
            </div>
            <!--.page_content_wrap-->
			<div class="page_content_wrap page_paddings_no">
               <!--.content-->
                <div class="content">
                    <!--.post_item .post_item_single .page .type-page-->
                    <article class="post_item post_item_single page type-page">
                        <!--.post_content -->
                        <div class="post_content">
                            <section class="no-col-padding">
                                <div class="container-fluid sc_a-us-bg-type-a ">    
                                    <div class="content_container">
                                        <div class="columns_wrap">
                                                <div class="column_container column-1_1">
                                                    <div class="column-inner">
                                                        <div class="m_wrapper">
                                                            <div class="columns_wrap sc_columns columns_nofluid sc_columns_count_9 sc_a-us-margin-type-a">
                                                                <div class="column-5_9 sc_column_item odd first">
                                                                    <h6 class="sc_title sc_title_regular margin_top_null sc_a-us-margin-type-b">à propos de nous</h6>
                                                                    <h2 class="sc_title sc_title_iconed margin_top_tiny margin_bottom_null sc_a-us-margin-type-c fsz_cust"><?php echo $about[0]['title'] ?><span class="sc_title_icon sc_title_icon_bottom  sc_title_icon_small"><img src="images/vector-smart-object-copy-12.png" alt="" /></span></h2>
                                                                    <div class="m_text_column m_content_element ">
                                                                        <div class="m_wrapper">
                                                                            <p><?php echo $about[0]['content'] ?></p>
                                                                        </div>
                                                                    </div> 	</div><div class="column-4_9 sc_column_item even">
                                                                    <div class="cq-coverslider  navigation-overlay-right tinyshadow grapefruit " data-imagemaxheight="300" data-buttonbackground="" data-buttonhoverbackground="#222F46" data-contentbackground="" data-contentcolor="" data-arrowcolor="" data-arrowhovercolor="" data-delaytime="2">
                                                                        <div class="cq-coverslider-area btn-large square">
                                                                            <div class="cq-coverslider-cover">
                                                                                <div class="cq-coverslider-itemcontainer">
																				<?php foreach($slide as $image){ ?>
                                                                                    <div class="cq-coverslider-imageitem"><img src="images/<?php echo $image['image'] ?>" class="cq-coverslider-image" alt="<?php echo $image['alt'] ?>"></div>
																				<?php } ?>
                                                                                </div>
                                                                            </div>
                                                                            <div class="cq-coverslider-content">
                                                                                <div class="cq-coverslider-contentitem"></div>
                                                                                <div class="cq-coverslider-contentitem"></div>
                                                                                <div class="cq-coverslider-contentitem"></div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="cq-coverslider-navigation btn-large">
                                                                            <div class="coverslider-navigation-prev"><i class="cq-coverslider-icon entypo-icon entypo-icon-left-open-big"></i></div><div class="coverslider-navigation-next"><i class="cq-coverslider-icon entypo-icon entypo-icon-right-open-big"></i></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                            </section>      
									
                        </div>
                        <!-- end .post_content -->
                    </article>
                   <!--end .post_item .post_item_single .page .type-page-->
                </div>
                <!-- end .content-->
            </div>
            <!-- end .page_content_wrap-->
		    <!-- .footer_wrap -->
            
            <!-- end .footer_wrap -->
            <?php include('footer.php')?>
        </div>
        <!-- end .page_wrap -->
    </div>
    <!-- end .body_wrap -->
    <a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>
    
    <script type='text/javascript' src='js/vendor/jquery-3.1.1.js'></script>
    <script type='text/javascript' src='js/vendor/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='js/vendor/photostack/modernizr.min.js'></script>
    <script type='text/javascript' src='js/vendor/superfish.js'></script>
    <script type="text/javascript" src="js/custom/_main.js"></script>
    <script type='text/javascript' src='js/custom/core.utils.js'></script>
    <script type='text/javascript' src='js/custom/core.init.js'></script>
    <script type='text/javascript' src='js/custom/template.init.js'></script>
    <script type='text/javascript' src='js/custom/template.shortcodes.js'></script>
    <script type='text/javascript' src='js/vendor/magnific/jquery.magnific-popup.min.js'></script>
    <script type='text/javascript' src='js/vendor/core.messages/core.messages.js'></script> 
    <script type='text/javascript' src='js/vendor/testimonialcarousel/slick/slick.min.js'></script> 
    <script type='text/javascript' src='js/vendor/coverslider/js/init.min.js'></script>
    <script type='text/javascript' src='js/vendor/swiper/swiper.js'></script>
    <script type='text/javascript' src='js/vendor/isotope.pkgd.min.js'></script>

</body>

<!-- Mirrored from lovestory-html.themerex.net/about-us.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 31 Mar 2018 12:59:14 GMT -->
</html>