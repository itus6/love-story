<?php
/**
 * Created by PhpStorm.
 * User: Harivola Fidisoa RAF
 * Date: 31/03/2018
 * Time: 20:01
 */
 //require('functions.php');
 $logo = findLogo();
?>
<!DOCTYPE html>
<html lang="en-US" class="scheme_original">

<!-- Mirrored from lovestory-html.themerex.net/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 31 Mar 2018 12:44:16 GMT -->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no">
    <title>LoveStory – Organisateur de Mariage</title>
    <link rel="icon" type="image/x-icon" href="images/favicons.png" />
    <link property="stylesheet" rel='stylesheet' href='css/fontello/css/fontello.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='css/entypo/entypo.min.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='css/style.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='css/template.shortcodes.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='css/template.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='css/core.animation.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/magnific/magnific-popup.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/woocommerce/assets/css/woocommerce-smallscreen.css' type='text/css' media='only screen and (max-width: 768px)' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/woocommerce/assets/css/woocommerce-layout.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/woocommerce/assets/css/woocommerce.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/woocommerce/assets/css/plugin.woocommerce.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/essential-grid/public/assets/css/lightbox.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/essential-grid/public/assets/css/settings.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/revslider/public/assets/css/settings.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/swiper/swiper.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/mediaelement/mediaelementplayer.min.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/mediaelement/wp-mediaelement.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/coverslider/css/style.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/testimonialcarousel/slick/slick.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='css/responsive.css' type='text/css' media='all' />
</head>

<body class="home page  body_filled  scheme_original">
<!--.body_wrap -->
<div class="body_wrap">
    <!--.page_wrap -->
    <div class="page_wrap">
        <div class="top_panel_fixed_wrap"></div>
        <header class="top_panel_wrap top_panel_style_2 scheme_original">
            <div class="top_panel_wrap_inner top_panel_inner_style_2 top_panel_position_above">
                <d  iv class="top_panel_middle">
                    <div class="content_wrap">
                        <div class="columns_wrap columns_fluid">
                            <div class="column-1_5 contact_field contact_phone">

                            </div>
                            <div class="column-3_5 contact_logo">
                                <div class="logo">
                                    <a href="accueil"><img src="images/<?php echo $logo[0]['image'] ?>" class="logo_main" alt="<?php echo $logo[0]['alt'] ?>">
                                        <img src="images/<?php echo $logo[0]['image'] ?>" class="logo_fixed" alt="<?php echo $logo[0]['alt'] ?>"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
