<?php
/**
 * Created by PhpStorm.
 * User: Harivola Fidisoa RAF
 * Date: 31/03/2018
 * Time: 20:11
 */
?>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no">
    <title>LoveStory – Wedding Planner</title>
    <link rel="icon" type="image/x-icon" href="images/favicons.png" />
    <link property="stylesheet" rel='stylesheet' href='css/fontello/css/fontello.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='css/entypo/entypo.min.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='css/style.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='css/template.shortcodes.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='css/template.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='css/core.animation.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/magnific/magnific-popup.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/woocommerce/assets/css/woocommerce-smallscreen.css' type='text/css' media='only screen and (max-width: 768px)' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/woocommerce/assets/css/woocommerce-layout.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/woocommerce/assets/css/woocommerce.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/woocommerce/assets/css/plugin.woocommerce.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/essential-grid/public/assets/css/lightbox.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/essential-grid/public/assets/css/settings.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/revslider/public/assets/css/settings.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/swiper/swiper.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/mediaelement/mediaelementplayer.min.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/mediaelement/wp-mediaelement.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/coverslider/css/style.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/testimonialcarousel/slick/slick.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='css/responsive.css' type='text/css' media='all' />
</head>
