<?php
 require('functions.php');
?>
<?php include ('logo.php')?>
                    <div class="top_panel_bottom">
                        <div class="content_wrap clearfix">
                            <nav class="menu_main_nav_area menu_hover_fade">
                                <ul id="menu_main" class="menu_main_nav">
                                    <li class="menu-item">
                                        <a href="index.php"><span>Accueil</span></a>
                                    </li>
                                    <li class="menu-item"><a href="nous-connaitre.php"><span>&Agrave; propos</span></a>
                                        
                                    </li>
                                    
                                    <li class="menu-item"><a href="notre-equipe.php"><span>&Eacute;quipe</span></a></li>
                                    <li class="menu-item current-menu-ancestor"><a href="articles.php"><span>Articles</span></a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
            
            <div class="top_panel_title title_present">
                <div class="top_panel_title_inner title_present bg-breadcrumbs">
                    <div class="content_wrap">
                        <h1 class="page_title">Sweet Moment of Joy</h1>
                        <div class="breadcrumbs"><a class="breadcrumbs_item home" href="index.html">Home</a><span class="breadcrumbs_delimiter"></span><a class="breadcrumbs_item all" href="blog-classic.html">Blog Classic</a><span class="breadcrumbs_delimiter"></span><a class="breadcrumbs_item cat_post" href="blog-standart-post.html">Outside Ceremony Pros</a><span class="breadcrumbs_delimiter"></span><span class="breadcrumbs_item current">Sweet Moment of Joy</span></div>
                    </div>
                </div>
            </div>
            <!-- .page_content_wrap> -->
            <div class="page_content_wrap page_paddings_yes">
                <!--.content_wrap-->
                <div class="content_wrap">
                    <!--.content -->
                    <div class="content">
                        <!-- .post_item .post .type-post -->
                        <article class="post_item post type-post">
                            <!--.post_content -->
                            <div class="post_content">
                                <section class="no-col-padding">                              
                                    <div class="container">
                                        <div class="columns_wrap">
                                            <div class="column_container column-1_1">
                                                <div class="column-inner">
                                                    <div class="m_wrapper">
                                                        <div class="post_info"> <span class="post_info_item post_info_posted"> <a href="#" class="post_info_date date updated"  content="2016-08-02 08:29:39">Aug 2, 2016</a></span> <span class="post_info_item post_info_posted_by vcard">By <a href="#" class="post_info_author">Cindy Jefferson</a></span> <span class="post_info_item post_info_counters">	<a class="post_counters_item post_counters_comments" title="Comments - 0" href="#"><span class="post_counters_number">0 comments</span></a></span>
                                                        </div>
                                                        <div class="m_text_column m_content_element ">
                                                            <div class="m_wrapper">
                                                                <div  class='gallery blog-gallery-1'>
                                                                    <dl class='gallery-item'>
                                                                        <dt class='gallery-icon landscape'>
                                                                            <a href='#'><img src="images/post-5-gallery.jpg" class="attachment-thumbnail size-thumbnail" alt="post-5"/></a>
                                                                        </dt>
                                                                    </dl>
                                                                    <dl class='gallery-item'>
                                                                        <dt class='gallery-icon landscape'>
                                                                            <a href='#'><img  src="images/post-3.jpg" class="attachment-thumbnail size-thumbnail" alt="post-3"/></a>
                                                                        </dt>
                                                                    </dl>
                                                                    <dl class='gallery-item'>
                                                                        <dt class='gallery-icon landscape'>
                                                                            <a href='#'><img  src="images/post-8.jpg" class="attachment-thumbnail size-thumbnail" alt="post-8"/></a>
                                                                        </dt>
                                                                    </dl>
                                                                    <br/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>	
                                </section>               
                                <section class="no-col-padding">                              
                                    <div class="container">
                                        <div class="columns_wrap">
                                            <div class="column_container column-1_1">
                                                <div class="column-inner">
                                                    <div class="m_wrapper">
                                                         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc porta fringilla ullamcorper. Morbi felis orci, lacinia a velit et, sodales condimentum metus. Nulla non fermentum nisl. Maecenas id molestie turpis, sit amet feugiat lorem. Curabitur sed erat vel tellus hendrerit tincidunt. Sed arcu tortor, sollicitudin ac lectus sed, rhoncus iaculis lectus. Ut efficitur feugiat enim a euismod. Mauris suscipit vehicula imperdiet.</p>
                                                         <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut tristique pretium tellus, sed fermentum est vestibulum id. Aenean semper, odio sed fringilla blandit, nisl nulla placerat mauris, sit amet commodo mi turpis at libero. Curabitur varius eros et lacus rutrum consequat. Mauris sollicitudin enim condimentum, luctus justo non, molestie nisl. Aenean et egestas nulla. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce gravida, ligula non molestie tristique, justo elit blandit risus, blandit maximus augue magna accumsan ante. Aliquam bibendum lacus quis nulla dignissim faucibus. Sed mauris enim, bibendum at purus aliquet, maximus molestie tortor. Sed faucibus et tellus eu sollicitudin. Sed fringilla malesuada luctus.</p>
                                                         <div class="post_info post_info_bottom"> <span class="post_info_item post_info_tags">Tags: <a class="post_tag_link" href="#">happy</a>, <a class="post_tag_link" href="#">moments</a></span> </div>
                                                         <div class="post_info post_info_bottom post_info_share post_info_share_horizontal">
                                                            <div class="sc_socials sc_socials_size_tiny sc_socials_share sc_socials_dir_horizontal"><span class="share_caption">Share:</span><div class="sc_socials_item social_item_popup"><a href="#" class="social_icons social_facebook" data-link="#"><span class="icon-facebook"></span></a></div><div class="sc_socials_item social_item_popup"><a href="#" class="social_icons social_twitter" data-link="#"><span class="icon-twitter"></span></a></div><div class="sc_socials_item social_item_popup"><a href="#" class="social_icons social_tumblr" data-link="#"><span class="icon-tumblr"></span></a></div><div class="sc_socials_item social_item_popup"><a href="#" class="social_icons social_gplus" data-link="#"><span class="icon-gplus"></span></a></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>	
                                </section>
                                <section class="post_author author vcard">
                                    <div class="post_author_avatar">
                                        <a href="#"><img alt='' src='images/avatar.jpg' srcset='images/avatar.jpg'/></a>
                                    </div>
                                    <h6 class="post_author_title">About <span><a href="#" class="fn">Cindy Jefferson</a></span></h6>
                                    <div class="post_author_info"> Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. </div>
                                </section>
                            </div>
                            <!-- end .post_content -->                        
                        </article>
                        <!--   end .post_item .post .type-post -->

                        <!-- .comments_wrap -->
                        <section class="comments_wrap">
                           <!-- .comments_form_wrap -->
                            <div class="comments_form_wrap">
                                <h2 class="section_title comments_form_title">Add Comment</h2>
                                <div class="comments_form">
                                    <!-- .comment-respond -->
                                    <div class="comment-respond">
                                        <h3 class="comment-reply-title"> <small><a rel="nofollow" href="#">Cancel reply</a></small></h3>
                                        <form action="#" method="post"  class="comment-form sc_input_hover_default">
                                            <p class="comments_notes">Your email address will not be published. Required fields are marked *</p>
                                            <div class="comments_field comments_author">
                                                <input name="author" type="text" placeholder="Name *" value="" />
                                            </div>
                                            <div class="comments_field comments_email">
                                                <input name="email" type="text" placeholder="Email *" value="" />
                                            </div>
                                            <div class="comments_field comments_site">
                                                <input name="url" type="text" placeholder="Website" value=""  />
                                            </div>
                                            <div class="comments_field comments_message">
                                                <textarea name="comment" placeholder="Comment"></textarea>
                                            </div>
                                            <p class="form-submit">
                                                <input name="submit" type="submit" class="submit" value="Post Comment" />
                                            </p>
                                        </form>
                                    </div>
                                    <!-- end .comment-respond -->
                                </div>
                            </div>
                            <!-- end .comments_form_wrap -->
                        </section>
                        <!-- end .comments_wrap -->
                    </div>
                    <!-- end .content -->

                    <!-- .sidebar -->
                    
                    <!-- end .sidebar -->

                </div>
                <!--end .content_wrap-->
            </div>
            <!-- end .page_content_wrap> -->

            
            <!-- end .footer_wrap -->
            <?php include('footer.php')?>
        </div>
        <!-- end .page_wrap -->
    </div>
    <!-- end .body_wrap -->
    <a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>
    
    <script type='text/javascript' src='js/vendor/jquery-3.1.1.js'></script>
    <script type='text/javascript' src='js/vendor/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='js/vendor/photostack/modernizr.min.js'></script>
    <script type='text/javascript' src='js/vendor/superfish.js'></script>
    <script type="text/javascript" src="js/custom/_main.js"></script>
    <script type='text/javascript' src='js/custom/core.utils.js'></script>
    <script type='text/javascript' src='js/custom/core.init.js'></script>
    <script type='text/javascript' src='js/custom/template.init.js'></script>
    <script type='text/javascript' src='js/custom/template.shortcodes.js'></script>
    <script type='text/javascript' src='js/vendor/magnific/jquery.magnific-popup.min.js'></script>
    <script type='text/javascript' src='js/vendor/core.messages/core.messages.js'></script>
    
</body>

<!-- Mirrored from lovestory-html.themerex.net/blog-gallery-post.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 31 Mar 2018 13:08:15 GMT -->
</html>