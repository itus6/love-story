<?php
 require('functions.php');
 $slides = findSlide();
?>
<?php include ('logo.php')?>
                    <div class="top_panel_bottom">
                        <div class="content_wrap clearfix">
                            <nav class="menu_main_nav_area menu_hover_fade">
                                <ul id="menu_main" class="menu_main_nav">
                                    <li class="menu-item  menu-item-has-children current-menu-ancestor">
                                        <a href="accueil"><span>Accueil</span></a>
                                    </li>
                                    <li class="menu-item"><a href="nous-connaitre"><span>&Agrave; propos</span></a>
                                        
                                    </li>
                                    
                                    <li class="menu-item"><a href="notre-equipe"><span>&Eacute;quipe</span></a></li>
                                    
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
            
        
            <section class="slider_wrap slider_fullwide slider_engine_revo slider_alias_Homeslider mainslider_1">
       
                <div id="mainslider_1" class="rev_slider_wrapper fullwidthbanner-container revslider-global-param " data-source="gallery">
                    <!-- START REVOLUTION SLIDER 5.3.0.2 fullwidth mode -->
                    <div id="rev_slider_2_1" class="rev_slider fullwidthabanner" data-version="5.3.0.2">
                        <ul>
                            <!-- SLIDE  -->
							<?php foreach($slides as $slide){ ?>
                            <li data-index="rs-5" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="images/slide1-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                <!-- MAIN IMAGE -->
								
                                <img src="images/<?php echo $slide['image'] ?>" alt="<?php echo $slide['alt'] ?>" title="<?php echo $slide['image'] ?>" width="1920" height="820" data-bgposition="center center" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="110" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" class="rev-slidebg" data-no-retina>
                                <!-- LAYERS -->
                                <!-- LAYER NR. 1 -->
                                <div class="tp-caption Title-slider tp-resizeme revslider-param-type-a" 
                                     id="slide-5-layer-1" 
                                     data-x="center" 
                                     data-hoffset="" 
                                     data-y="center" 
                                     data-voffset="-50" 
                                     data-width="['auto']" 
                                     data-height="['auto']" 
                                     data-type="text" 
                                     data-responsive_offset="on"
                                     data-frames='[{"from":"y:50px;opacity:0;","speed":700,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' 
                                     data-textAlign="['center','center','center','center']" 
                                     data-paddingtop="[0,0,0,0]" 
                                     data-paddingright="[0,0,0,0]" 
                                     data-paddingbottom="[0,0,0,0]" 
                                     data-paddingleft="[0,0,0,0]"><?php echo $slide['texts'] ?>
                                </div>
                                <!-- LAYER NR. 2 -->
                                <div class="tp-caption Divider-slider tp-resizeme revslider-param-type-b" 
                                     id="slide-5-layer-2" 
                                     data-x="center" 
                                     data-hoffset="1" 
                                     data-y="center" 
                                     data-voffset="49" 
                                     data-width="['none','none','none','none']" 
                                     data-height="['none','none','none','none']" 
                                     data-type="image" 
                                     data-responsive_offset="on" 
                                     data-frames='[{"from":"y:50px;opacity:0;","speed":700,"to":"o:1;","delay":1000,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' 
                                     data-textAlign="['left','left','left','left']" 
                                     data-paddingtop="[0,0,0,0]" 
                                     data-paddingright="[0,0,0,0]" 
                                     data-paddingbottom="[0,0,0,0]" 
                                     data-paddingleft="[0,0,0,0]">
                                     <img src="images/divider_white.png" alt="" data-ww="260px" data-hh="26px" width="260" height="26" data-no-retina>
                                </div>
                            </li>
							<?php } ?>
                        </ul>
                        <div class="tp-bannertimer tp-bottom hidden"></div>	
                    </div>
                </div>
                <!-- END REVOLUTION SLIDER -->
            </section>
		    <div class="page_content_wrap page_paddings_no">
                <div class="content">
                    <article class="post_item post_item_single page type-page">
                        <div class="post_content">
                            <section class="no-col-padding">
                                <div class="container-fluid sc_home-bg-type-a">    
                                    <div class="content_container">
                                        <div class="columns_wrap">
                                            <div class="column_container column-1_1">
                                                <div class="column-inner">
                                                    <div class="m_wrapper">
                                                        <div  class="sc_call_to_action sc_call_to_action_accented sc_call_to_action_style_2 sc_call_to_action_align_center w100">
                                                            <div class="sc_call_to_action_info">
                                                                <h2 class="sc_call_to_action_title sc_item_title sc_item_title_without_descr">Confiez-nous Votre Grand Jour!</h2>
                                                            </div>
                                                            <div class="sc_call_to_action_buttons sc_item_buttons">
                                                                </div>
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </section>       
                            
                            
                    <!-- end .post_item .post_item_single .page .type-page -->
                </div>
                <!--end .content-->
            </div>
            <!--end .page_content_wrap-->
            
            <!--.contacts_wrap -->
            <footer class="contacts_wrap scheme_original">
               <!--.contacts_wrap_inner -->
                <div class="contacts_wrap_inner">
                   <!--.content_wrap -->
                    <div class="content_wrap">
                        <div class="contacts_address">
                            <address class="address_right address_block">
                                <span class="sc_icon icon-icon4"></span>
                                <p class="address_title">Adresse</p><p>123, Lalana Ramaromanana, Antananarivo 101<br></p>			
                            </address><address class="address_center phone_block">
                                <span class="sc_icon icon-icon5"></span>
                                <p class="address_title">Téléphone</p><p>+261 33 33 333 33</p>				
                            </address><address class="address_left email_block">
                                <span class="sc_icon icon-icon6"></span>
                                <p class="address_title">Adresse email</p><p>info@love-story.mg </p>	
                            </address>
                        </div>
                    </div>	
                    <!-- end .content_wrap -->
                </div>	
                <!-- end .contacts_wrap_inner -->
			</footer>
			<!-- end .contacts_wrap -->
			
			<!--.footer_wrap -->
			<?php include('footer.php')?>
            <!-- end .footer_wrap -->
				 
			
		</div>	
		<!-- end .page_wrap -->
	</div>		
   <!-- end .body_wrap -->
    <a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>
    
    <script type='text/javascript' src='js/vendor/jquery-3.1.1.js'></script>
    <script type='text/javascript' src='js/vendor/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='js/vendor/photostack/modernizr.min.js'></script>
    <script type='text/javascript' src='js/vendor/superfish.js'></script>
    <script type='text/javascript' src='js/vendor/essential-grid/public/assets/js/lightbox.js'></script>
    <script type='text/javascript' src='js/vendor/essential-grid/public/assets/js/jquery.themepunch.tools.min.js'></script>
    <script type='text/javascript' src='js/vendor/essential-grid/public/assets/js/jquery.themepunch.essential.min.js'></script>
    <script type='text/javascript' src='js/vendor/revslider/public/assets/js/jquery.themepunch.revolution.min.js'></script>
    <script type="text/javascript" src="js/custom/_main.js"></script>
    <script type='text/javascript' src='js/custom/core.utils.js'></script>
    <script type='text/javascript' src='js/custom/core.init.js'></script>
    <script type='text/javascript' src='js/custom/template.init.js'></script>
    <script type='text/javascript' src='js/custom/template.shortcodes.js'></script>
    <script type='text/javascript' src='js/vendor/magnific/jquery.magnific-popup.min.js'></script>
    <script type='text/javascript' src='js/vendor/core.messages/core.messages.js'></script> 
    <script type='text/javascript' src='js/vendor/testimonialcarousel/slick/slick.min.js'></script> 
    <script type='text/javascript' src='js/vendor/coverslider/js/init.min.js'></script>
    <script type='text/javascript' src='js/vendor/swiper/swiper.js'></script>
    <script type='text/javascript' src='js/vendor/isotope.pkgd.min.js'></script>
   
</body>

<!-- Mirrored from lovestory-html.themerex.net/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 31 Mar 2018 12:50:19 GMT -->
</html>