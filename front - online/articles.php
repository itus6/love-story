<?php
 require('functions.php');
?>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no">
    <title>Blog Portfolio 2 Columns &#8211; LoveStory – Wedding Planner</title>
    <link rel="icon" type="image/x-icon" href="images/favicons.png"/>
    <link property="stylesheet" rel='stylesheet' href='css/fontello/css/fontello.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='css/style.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='css/template.shortcodes.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='css/template.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='css/core.animation.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='css/core.portfolio.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/magnific/magnific-popup.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/woocommerce/assets/css/woocommerce-smallscreen.css' type='text/css' media='only screen and (max-width: 768px)' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/woocommerce/assets/css/woocommerce-layout.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/woocommerce/assets/css/woocommerce.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='js/vendor/woocommerce/assets/css/plugin.woocommerce.css' type='text/css' media='all' />
    <link property="stylesheet" rel='stylesheet' href='css/responsive.css' type='text/css' media='all' /> 
</head>
<?php include ('logo.php')?>
                    <div class="top_panel_bottom">
                        <div class="content_wrap clearfix">
                            <nav class="menu_main_nav_area menu_hover_fade">
                                <ul id="menu_main" class="menu_main_nav">
                                    <li class="menu-item  ">
                                        <a href="index.php"><span>Accueil</span></a>
                                    </li>
                                    <li class="menu-item"><a href="nous-connaitre.php"><span>&Agrave; propos</span></a>
                                        
                                    </li>
                                    
                                    <li class="menu-item"><a href="notre-equipe.php"><span>&Eacute;quipe</span></a></li>
                                    <li class="menu-item current-menu-ancestor"><a href="articles.php"><span>Articles</span></a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
            
            <div class="top_panel_title title_present">
                <div class="top_panel_title_inner title_present bg-breadcrumbs">
                    <div class="content_wrap">
                        <h1 class="page_title">Blog Portfolio 2 Columns</h1><div class="breadcrumbs"><a class="breadcrumbs_item home" href="#">Home</a><span class="breadcrumbs_delimiter"></span><span class="breadcrumbs_item current">Blog Portfolio 2 Columns</span></div>	
                    </div>
                </div>
            </div>
            <div class="page_content_wrap page_paddings_yes">
                <div class="content_wrap">
                    <div class="content">
                        <article class="post_item post_item_single page type-page">
                            <div class="post_content">
                                <section class="no-col-padding">
                                    <div class="container">
                                        <div class="columns_wrap">
                                            <div class="column_container column-1_1">
                                                <div class="column-inner">
                                                    <div class="m_wrapper">
                                                        <div  class="sc_blogger">
                                                            <div class="isotope_wrap" data-columns="2">
                                                                <!--.isotope_item -->
                                                                <div class="isotope_item isotope_item_portfolio isotope_column_2">
                                                                    <!--.post_item -->
                                                                    <div class="post_item odd">
                                                                        <!-- .post_content -->
                                                                        <div class="post_content isotope_item_content ih-item colored square effect_shift left_to_right">
                                                                            <div class="post_featured img ">
                                                                                <a href="blog-standart-post.html"><img class="post-image"  alt="The First Day of the Rest of Their Lives" src="images/post-1-370x284.jpg"></a>
                                                                            </div>
                                                                             <!--.info -->
                                                                            <div class="post_info_wrap info">
                                                                               <!--.info-back  -->
                                                                                <div class="info-back">
                                                                                    <h4 class="post_title"><a href="article.php">The First Day of the Rest of Their Lives</a></h4>
                                                                                    <div class="post_descr">
                                                                                        <p class="post_info"> <span class="post_info_item post_info_posted"> <a href="#" class="post_info_date">Aug 15, 2016</a></span> <span class="post_info_item post_info_counters">	<a class="post_counters_item post_counters_comments" title="Comments - 0" href="#"><span class="post_counters_number">0 comments</span></a>
                                                                                            </span>
                                                                                        </p>
                                                                                        <p><a href="blog-standart-post.html">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc porta fringilla ullamcorper. Morbi felis orci&#8230;</a></p>
                                                                                        <p class="post_buttons"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <!--end .info-back  -->
                                                                            </div>
                                                                            <!--end .info -->
                                                                        </div>
                                                                        <!-- end .post_content -->
                                                                    </div>
                                                                    <!-- end .post_item -->
                                                                </div>
                                                                <!-- end .isotope_item -->

                                                                <!--.isotope_item -->
                                                                <div class="isotope_item isotope_item_portfolio isotope_column_2">
                                                                    <!--.post_item -->
                                                                    <div class="post_item even">
                                                                       <!--.post_content -->
                                                                        <div class="post_content isotope_item_content ih-item colored square effect_shift left_to_right">
                                                                            <div class="post_featured img ">
                                                                                <a href="blog-standart-post.html"><img class="post-image"  alt="Colors and Textures for Summer Weddings" src="images/post-2-370x444.jpg"></a>
                                                                            </div>
                                                                            <!--.info -->
                                                                            <div class="post_info_wrap info">
                                                                                <!--.info-back  -->
                                                                                <div class="info-back">
                                                                                    <h4 class="post_title"><a href="blog-standart-post.html">Colors and Textures for Summer Weddings</a></h4>
                                                                                    <div class="post_descr">
                                                                                        <p class="post_info"> <span class="post_info_item post_info_posted"> <a href="#" class="post_info_date">Jun 21, 2016</a></span> <span class="post_info_item post_info_counters">	<a class="post_counters_item post_counters_comments" title="Comments - 0" href="#"><span class="post_counters_number">0 comments</span></a>
                                                                                            </span>
                                                                                        </p>
                                                                                        <p><a href="blog-standart-post.html">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc porta fringilla ullamcorper. Morbi felis orci&#8230;</a></p>
                                                                                        <p class="post_buttons"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <!--end .info-back  -->
                                                                            </div>
                                                                             <!--end .info -->
                                                                        </div>
                                                                        <!-- end .post_content -->
                                                                    </div>
                                                                    <!-- end .post_item -->
                                                                </div>
                                                                <!-- end .isotope_item -->

                                                                <!--.isotope_item -->
                                                                <div class="isotope_item isotope_item_portfolio isotope_column_2">
                                                                    <!--.post_item -->
                                                                    <div class="post_item odd">
                                                                        <!--.post_content -->
                                                                        <div class="post_content isotope_item_content ih-item colored square effect_shift left_to_right">
                                                                            <div class="post_featured img ">
                                                                                <a href="blog-standart-post.html"><img class="post-image" alt="Happily Ever after or Yes! I Will" src="images/post-3-370x246.jpg"></a>
                                                                            </div>
                                                                            <!--.info -->
                                                                            <div class="post_info_wrap info">
                                                                                <!--.info-back  -->
                                                                                <div class="info-back">
                                                                                    <h4 class="post_title"><a href="blog-standart-post.html">Happily Ever after or Yes! I Will</a></h4>
                                                                                    <div class="post_descr">
                                                                                        <p class="post_info"> <span class="post_info_item post_info_posted"> <a href="#" class="post_info_date">Jun 15, 2016</a></span> <span class="post_info_item post_info_counters">	<a class="post_counters_item post_counters_comments" title="Comments - 0" href="#"><span class="post_counters_number">0 comments</span></a>
                                                                                            </span>
                                                                                        </p>
                                                                                        <p><a href="blog-standart-post.html">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc porta fringilla ullamcorper. Morbi felis orci&#8230;</a></p>
                                                                                        <p class="post_buttons"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <!--end .info-back  -->
                                                                            </div>
                                                                            <!--end .info -->
                                                                        </div>
                                                                        <!-- end .post_content -->
                                                                    </div>
                                                                    <!-- end .post_item -->
                                                                </div>
                                                                <!-- end .isotope_item -->

                                                                <!--.isotope_item --> 
                                                                <div class="isotope_item isotope_item_portfolio isotope_column_2">
                                                                    <!--.post_item -->
                                                                    <div class="post_item even">
                                                                        <!--.post_content -->
                                                                        <div class="post_content isotope_item_content ih-item colored square effect_shift left_to_right">
                                                                            <div class="post_featured img ">
                                                                                <a href="blog-standart-post.html"><img class="post-image" alt="3 Types of Long Lasting Make Up" src="images/post-4-370x224.jpg"></a>
                                                                            </div>
                                                                            <!--.info -->
                                                                            <div class="post_info_wrap info">
                                                                                <!--.info-back  -->
                                                                                <div class="info-back">
                                                                                    <h4 class="post_title"><a href="blog-standart-post.html">3 Types of Long Lasting Make Up</a></h4>
                                                                                    <div class="post_descr">
                                                                                        <p class="post_info"> <span class="post_info_item post_info_posted"> <a href="#" class="post_info_date">Jun 4, 2016</a></span> <span class="post_info_item post_info_counters">	<a class="post_counters_item post_counters_comments" title="Comments - 0" href="#"><span class="post_counters_number">0 comments</span></a>
                                                                                            </span>
                                                                                        </p>
                                                                                        <p><a href="blog-standart-post.html">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc porta fringilla ullamcorper. Morbi felis orci&#8230;</a></p>
                                                                                        <p class="post_buttons"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <!--end .info-back  -->
                                                                            </div>
                                                                            <!--end .info -->
                                                                        </div>
                                                                        <!-- end .post_content -->
                                                                    </div>
                                                                    <!-- end .post_item -->
                                                                </div>
                                                                <!-- end .isotope_item -->

                                                                <!--.isotope_item -->
                                                                <div class="isotope_item isotope_item_portfolio isotope_column_2">
                                                                    <!--.post_item -->
                                                                    <div class="post_item odd">
                                                                        <!--.post_content -->
                                                                        <div class="post_content isotope_item_content ih-item colored square effect_shift left_to_right">
                                                                            <div class="post_featured img ">
                                                                                <a href="blog-standart-post.html"><img class="post-image" alt="Top Wedding Bakers in Your Area" src="images/post-5-370x322.jpg"></a>
                                                                            </div>
                                                                             <!--.info -->
                                                                            <div class="post_info_wrap info">
                                                                                <!--.info-back  -->
                                                                                <div class="info-back">
                                                                                    <h4 class="post_title"><a href="blog-standart-post.html">Top Wedding Bakers in Your Area</a></h4>
                                                                                    <div class="post_descr">
                                                                                        <p class="post_info"> <span class="post_info_item post_info_posted"> <a href="#" class="post_info_date">May 30, 2016</a></span> <span class="post_info_item post_info_counters">	<a class="post_counters_item post_counters_comments" title="Comments - 0" href="#"><span class="post_counters_number">0 comments</span></a>
                                                                                            </span>
                                                                                        </p>
                                                                                        <p><a href="blog-standart-post.html">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc porta fringilla ullamcorper. Morbi felis orci&#8230;</a></p>
                                                                                        <p class="post_buttons"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <!--end .info-back  -->
                                                                            </div>
                                                                            <!--end .info -->
                                                                        </div>
                                                                        <!-- end .post_content -->
                                                                    </div>
                                                                    <!-- end .post_item -->
                                                                </div>
                                                                <!-- end .isotope_item -->

                                                                <!--.isotope_item -->
                                                                <div class="isotope_item isotope_item_portfolio isotope_column_2">
                                                                    <!--.post_item -->
                                                                    <div class="post_item even">
                                                                        <!--.post_content -->
                                                                        <div class="post_content isotope_item_content ih-item colored square effect_shift left_to_right">
                                                                            <div class="post_featured img ">
                                                                                <a href="blog-standart-post.html"><img class="post-image" alt="Table Arrangements from A to Z" src="images/post-6-370x281.jpg"></a>
                                                                            </div>
                                                                            <!--.info -->
                                                                            <div class="post_info_wrap info">
                                                                                <!--.info-back  -->
                                                                                <div class="info-back">
                                                                                    <h4 class="post_title"><a href="blog-standart-post.html">Table Arrangements from A to Z</a></h4>
                                                                                    <div class="post_descr">
                                                                                        <p class="post_info"> <span class="post_info_item post_info_posted"> <a href="#" class="post_info_date">May 20, 2016</a></span> <span class="post_info_item post_info_counters">	<a class="post_counters_item post_counters_comments" title="Comments - 0" href="#"><span class="post_counters_number">0 comments</span></a>
                                                                                            </span>
                                                                                        </p>
                                                                                        <p><a href="blog-standart-post.html">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc porta fringilla ullamcorper. Morbi felis orci&#8230;</a></p>
                                                                                        <p class="post_buttons"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <!--end .info-back  -->
                                                                            </div>
                                                                            <!--end .info -->
                                                                        </div>
                                                                        <!-- end .post_content -->
                                                                    </div>
                                                                    <!-- end .post_item -->
                                                                </div>
                                                                <!-- end .isotope_item -->

                                                                <!--.isotope_item -->
                                                                <div class="isotope_item isotope_item_portfolio isotope_column_2">
                                                                    <!--.post_item -->
                                                                    <div class="post_item odd">
                                                                        <!--.post_content -->
                                                                        <div class="post_content isotope_item_content ih-item colored square effect_shift left_to_right">
                                                                            <div class="post_featured img ">
                                                                                <a href="blog-standart-post.html"><img class="post-image" alt="Speacial Treats for the Guests" src="images/post-7-370x310.jpg"></a>
                                                                            </div>
                                                                            <!--.info -->
                                                                            <div class="post_info_wrap info">
                                                                                <!--.info-back  -->
                                                                                <div class="info-back">
                                                                                    <h4 class="post_title"><a href="blog-standart-post.html">Speacial Treats for the Guests</a></h4>
                                                                                    <div class="post_descr">
                                                                                        <p class="post_info"> <span class="post_info_item post_info_posted"> <a href="#" class="post_info_date">Apr 22, 2016</a></span> <span class="post_info_item post_info_counters">	<a class="post_counters_item post_counters_comments" title="Comments - 0" href="#"><span class="post_counters_number">0 comments</span></a>
                                                                                            </span>
                                                                                        </p>
                                                                                        <p><a href="blog-standart-post.html">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc porta fringilla ullamcorper. Morbi felis orci&#8230;</a></p>
                                                                                        <p class="post_buttons"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <!--end .info-back  -->
                                                                            </div>
                                                                            <!--end .info -->
                                                                        </div>
                                                                        <!-- end .post_content -->
                                                                    </div>
                                                                    <!-- end .post_item -->
                                                                </div>
                                                                <!-- end .isotope_item -->

                                                                <!--.isotope_item -->
                                                                <div class="isotope_item isotope_item_portfolio isotope_column_2">
                                                                    <!--.post_item -->
                                                                    <div class="post_item even last">
                                                                        <!--.post_content -->
                                                                        <div class="post_content isotope_item_content ih-item colored square effect_shift left_to_right">
                                                                            <div class="post_featured img ">
                                                                                <a href="blog-standart-post.html"><img class="post-image" alt="Beautiful Landscapes for Outside Ceremonies" src="images/post-8-370x246.jpg"></a>
                                                                            </div>
                                                                            <!--.info -->
                                                                            <div class="post_info_wrap info">
                                                                                <!--.info-back  -->
                                                                                <div class="info-back">
                                                                                    <h4 class="post_title"><a href="blog-standart-post.html">Beautiful Landscapes for Outside Ceremonies</a></h4>
                                                                                    <div class="post_descr">
                                                                                        <p class="post_info"> <span class="post_info_item post_info_posted"> <a href="#" class="post_info_date">Apr 14, 2016</a></span> <span class="post_info_item post_info_counters">	<a class="post_counters_item post_counters_comments" title="Comments - 0" href="#"><span class="post_counters_number">0 comments</span></a>
                                                                                            </span>
                                                                                        </p>
                                                                                        <p><a href="blog-standart-post.html">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc porta fringilla ullamcorper. Morbi felis orci&#8230;</a></p>
                                                                                        <p class="post_buttons"></p>
                                                                                    </div>
                                                                                </div>
                                                                                <!--end .info-back  -->
                                                                            </div>
                                                                            <!--end .info -->
                                                                        </div>
                                                                        <!-- end .post_content -->
                                                                    </div>
                                                                    <!-- end .post_item -->
                                                                </div>
                                                                <!-- end .isotope_item -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>	
                                </section>
                            </div>
                            <!-- end .post_content -->

                        </article>
                        <!-- end .post_item .post_item_singlepage .page .type-page  -->
                    </div>
                    <!-- end .content -->

                    <!-- .sidebar -->
                    
                    <!-- end .sidebar -->
                </div>
                <!-- end .content_wrap> -->
            </div>
            <!-- end .page_content_wrap -->
            <!-- .footer_wrap -->
            <?php include('footer.php')?>
              <!-- end .footer_wrap -->
            <div class="copyright_wrap copyright_style_text  scheme_original">
                <div class="copyright_wrap_inner">
                    <div class="content_wrap">
                        <div class="copyright_text">
                            <p>ThemeREX &copy; 2017 All Rights Reserved <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end .page_wrap -->
    </div>
    <!-- end .body_wrap -->
    <a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>
    
    <script type='text/javascript' src='js/vendor/jquery-3.1.1.js'></script>
    <script type='text/javascript' src='js/vendor/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='js/vendor/photostack/modernizr.min.js'></script>
    <script type='text/javascript' src='js/vendor/superfish.js'></script>
    <script type="text/javascript" src="js/custom/_main.js"></script>
    <script type='text/javascript' src='js/custom/core.utils.js'></script>
    <script type='text/javascript' src='js/custom/core.init.js'></script>
    <script type='text/javascript' src='js/custom/template.init.js'></script>
    <script type='text/javascript' src='js/custom/template.shortcodes.js'></script>
    <script type='text/javascript' src='js/vendor/magnific/jquery.magnific-popup.min.js'></script>
    <script type='text/javascript' src='js/vendor/jquery.isotope.min.js'></script>
    <script type='text/javascript' src='js/vendor/jquery.hoverdir.js'></script>
    <script type='text/javascript' src='js/vendor/core.messages/core.messages.js'></script>

</body>

<!-- Mirrored from lovestory-html.themerex.net/blog-portfolio-2-columns.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 31 Mar 2018 13:06:48 GMT -->
</html>
							