<?php
 require('functions.php');
 $id = $_GET['id'];
 $team = findTeamById($id);
?>
<?php include ('logo.php')?>
            <div class="top_panel_bottom">
                        <div class="content_wrap clearfix">
                            <nav class="menu_main_nav_area menu_hover_fade">
                                <ul id="menu_main" class="menu_main_nav">
                                    <li class="menu-item">
                                        <a href="accueil"><span>Accueil</span></a>
                                    </li>
                                    <li class="menu-item"><a href="nous-connaitre"><span>&Agrave; propos</span></a>
                                        
                                    </li>
                                    
                                    <li class="menu-item"><a href="notre-equipe"><span>&Eacute;quipe</span></a></li>
                                    
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
             <!-- .page_content_wrap--> 	        
            <div class="page_content_wrap page_paddings_no">
               <!--.content -->
                <div class="content">
                    <!-- .post_item .post_item_single_team -->
                    <article class="post_item post_item_single_team">
                        <!--.post_content-->
                        <div class="post_content">
                            <section class="no-col-padding">
                                <div class="content_container">
                                    <div class="columns_wrap">
                                        <div class="column_container column-1_1">
                                            <div class="column-inner">
                                                <div class="m_wrapper">
                                                    <div class="columns_wrap sc_columns columns_nofluid sc_columns_count_7 sc_our-team-single-column-margin">
                                                        <div class="column-3_7 sc_column_item odd first">
                                                            <figure class="sc_image  sc_image_shape_square"><img src="images/<?php echo $team[0]['image']?>" alt="<?php echo $team[0]['alt']?>" /></figure>
                                                        </div><div class="column-4_7 sc_column_item even">
                                                            <h4 class="sc_title sc_title_regular margin_bottom_tiny sc_our-team-single-title-margin"><?php echo $team[0]['name']?></h4>
                                                            <div class="m_text_column m_content_element ">
                                                                <div class="m_wrapper">
                                                                    <p><?php echo $team[0]['description']?></p>
                                                                    <p><span class="sc_our-team-single-adress-style" ><strong class="sc_our-team-single-custom-color">phone:</strong></span> <?php echo $team[0]['phone']?>
                                                                        <br /> <span class="sc_our-team-single-adress-style"><strong class="sc_our-team-single-custom-color">mail:</strong></span> <?php echo $team[0]['mail']?>
                                                                        <br /> <span class="sc_our-team-single-adress-style"><strong class="sc_our-team-single-custom-color">skype:</strong></span> <?php echo $team[0]['skype']?></p>
                                                                </div>
                                                            </div>
                                                            <div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny margin_top_medium">
                                                                <div class="sc_socials_item"><a href="<?php echo $team[0]['facebook']?>" class="social_icons social_facebook"><span class="icon-facebook"></span></a></div><div class="sc_socials_item"><a href="<?php echo $team[0]['twitter']?>" class="social_icons social_twitter"><span class="icon-twitter"></span></a></div><div class="sc_socials_item"><a href="<?php echo $team[0]['instagram']?>" class="social_icons social_instagramm"><span class="icon-instagramm"></span></a></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                            </section>
                            
                            
                        </div>
                        <!-- end .post_content-->
                    </article>
                    <!-- end .post_item .post_item_single_team -->
                </div>
                <!--end .content -->
            </div>
            <!-- end .page_content_wrap--> 
			<!-- .footer_wrap -->
            
              <!-- end .footer_wrap -->
            <?php include('footer.php');?>
        </div>
        <!-- end .page_wrap -->
    </div>
    <!-- end .body_wrap -->
    <a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>
 
    <script type='text/javascript' src='js/vendor/jquery-3.1.1.js'></script>
    <script type='text/javascript' src='js/vendor/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='js/vendor/photostack/modernizr.min.js'></script>
    <script type='text/javascript' src='js/vendor/superfish.js'></script>
    <script type="text/javascript" src="js/custom/_main.js"></script>
    <script type='text/javascript' src='js/custom/core.utils.js'></script>
    <script type='text/javascript' src='js/custom/core.init.js'></script>
    <script type='text/javascript' src='js/custom/template.init.js'></script>
    <script type='text/javascript' src='js/custom/template.shortcodes.js'></script>
    <script type='text/javascript' src='js/vendor/magnific/jquery.magnific-popup.min.js'></script>
    <script type='text/javascript' src='js/vendor/core.messages/core.messages.js'></script>
    <script type='text/javascript' src='js/vendor/swiper/swiper.js'></script>
 
</body>

<!-- Mirrored from lovestory-html.themerex.net/our-team-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 31 Mar 2018 13:07:14 GMT -->
</html>	     						